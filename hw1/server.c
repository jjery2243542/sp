#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define ERR_EXIT(a) { perror(a); exit(1); }
#define read_lock(fd, offset, whence, len) lock_reg(fd, F_SETLK, F_RDLCK, offset, whence, len)
#define write_lock(fd, offset, whence, len) lock_reg(fd, F_SETLK, F_WRLCK, offset, whence, len)
#define un_lock(fd, offset, whence, len) lock_reg(fd, F_SETLK, F_UNLCK, offset, whence, len)

typedef struct {
    char hostname[512];  // server's hostname
    unsigned short port;  // port to listen
    int listen_fd;  // fd to wait for a new connection
} server;

typedef struct {
    char host[512];  // client's host
    int conn_fd;  // fd to talk with client
    char buf[512];  // data sent by/to client
    size_t buf_len;  // bytes used by buf
    // you don't need to change this.
	int account;
    int wait_for_write;  // used by handle_read to know if the header is read or not.
} request;

server svr;  // server
request* requestP = NULL;  // point to a list of requests
int maxfd;  // size of open file descriptor table, size of request list

const char* accept_read_header = "ACCEPT_FROM_READ";
const char* accept_write_header = "ACCEPT_FROM_WRITE";
const char* reject_header = "REJECT\n";

// Forwards

static void init_server(unsigned short port);
// initailize a server, exit for error

static void init_request(request* reqP);
// initailize a request instance

static void free_request(request* reqP);
// free resources used by a request instance

static int handle_read(request* reqP);
// return 0: socket ended, request done.
// return 1: success, message (without header) got this time is in reqP->buf with reqP->buf_len bytes. read more until got <= 0.
// It's guaranteed that the header would be correctly set after the first read.
// error code:
// -1: client connection error

int lock_reg(int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
	struct flock lock;
	lock.l_type = type;
	lock.l_start = offset;
	lock.l_whence = whence;
	lock.l_len = len;

	return (fcntl(fd, cmd, &lock));
}

int main(int argc, char** argv) {
    int i, ret;

    struct sockaddr_in cliaddr;  // used by accept()
    int clilen;

    int conn_fd;  // fd for a new connection with client
    int file_fd;  // fd for file that we open for reading
    char buf[512];
    int buf_len;

    // Parse args.
    if (argc != 2) {
        fprintf(stderr, "usage: %s [port]\n", argv[0]);
        exit(1);
    }

    // Initialize server
    init_server((unsigned short) atoi(argv[1]));

    // Get file descripter table size and initize request table
    maxfd = getdtablesize();
    requestP = (request*) malloc(sizeof(request) * maxfd);
    if (requestP == NULL) {
        ERR_EXIT("out of memory allocating all requests");
    }
    for (i = 0; i < maxfd; i++) {
        init_request(&requestP[i]);
    }
    requestP[svr.listen_fd].conn_fd = svr.listen_fd;
    strcpy(requestP[svr.listen_fd].host, svr.hostname);

    // Loop for handling connections
    fprintf(stderr, "\nstarting on %.80s, port %d, fd %d, maxconn %d...\n", svr.hostname, svr.port, svr.listen_fd, maxfd);
		fd_set check_set, return_set;
		int max_fd;
		int id_occupied[20] = {0};
		FD_ZERO(&check_set);
		FD_SET(svr.listen_fd, &check_set);
		if ((file_fd = open("account_info", O_RDWR)) < 0)
			fprintf(stderr, "open error");
		max_fd = file_fd;
		while (1) {
			// TODO: Add IO multiplexing
			// Check new connection
			return_set = check_set;
			if (select(max_fd + 1, &return_set, NULL, NULL, NULL) == -1)
				fprintf(stderr, "select error");
			int flag = 1;
			int i;
			int recent_fd;
			for (i = svr.listen_fd; i <= max_fd && flag; i++) {
				if (FD_ISSET(i, &return_set)) {
					recent_fd = i;
					flag = 0;
				}
			}
			if (recent_fd == svr.listen_fd) {
				clilen = sizeof(cliaddr);
				conn_fd = accept(svr.listen_fd, (struct sockaddr*)&cliaddr, (socklen_t*)&clilen);
				if (conn_fd < 0) {
					if (errno == EINTR || errno == EAGAIN) continue;  // try again
					if (errno == ENFILE) {
						(void) fprintf(stderr, "out of file descriptor table ... (maxconn %d)\n", maxfd);
						continue;
					}
					ERR_EXIT("accept")
				}
				requestP[conn_fd].conn_fd = conn_fd;
				FD_SET(conn_fd, &check_set);
				if (conn_fd > max_fd)
					max_fd = conn_fd;
				strcpy(requestP[conn_fd].host, inet_ntoa(cliaddr.sin_addr));
				fprintf(stderr, "getting a new request... fd %d from %s\n", conn_fd, requestP[conn_fd].host);
				continue;
			}
			else {
				ret = handle_read(&requestP[recent_fd]);

				if (ret < 0) {
					fprintf(stderr, "bad request from %s\n", requestP[conn_fd].host);
					continue;
				}
				int money, id;
#ifdef READ_SERVER
				id = atoi(requestP[recent_fd].buf);
				if (lseek(file_fd, (id - 1) * 2 * sizeof(int)  + sizeof(int), SEEK_SET) == -1)
					fprintf(stderr, "lseek error\n");
				if (read_lock(file_fd, 0, SEEK_CUR, 4) < 0) {
						write(requestP[recent_fd].conn_fd, "This account is occupied.\n", 26);
						FD_CLR(recent_fd, &check_set);
				}
				else {
					if (read(file_fd, &money, sizeof(int)) != sizeof(int))
						fprintf(stderr, "read error\n");
					sprintf(buf,"Balance: %d\n", money);
					write(requestP[recent_fd].conn_fd, buf, strlen(buf));
					if (un_lock(file_fd, -4, SEEK_CUR, 4) < 0)
						fprintf(stderr, "unlock error");
					FD_CLR(recent_fd, &check_set);
				}
#else
				if (requestP[recent_fd].buf[0] == '+' || requestP[recent_fd].buf[0] == '-') {
					id = requestP[recent_fd].account;
					if (lseek(file_fd, (id - 1) * 2 * sizeof(int)  + sizeof(int), SEEK_SET) == -1)
						fprintf(stderr, "lseek error\n");
					if (read(file_fd, &money, sizeof(int)) != sizeof(int))
						fprintf(stderr, "read error\n");
					money += atoi(requestP[recent_fd].buf);
					if (money < 0)
						write(requestP[recent_fd].conn_fd, "Operation fail.\n", 16);
					else {
						if (lseek(file_fd, -4, SEEK_CUR) == -1)
							fprintf(stderr, "lseek error\n");
						if (write(file_fd, &money, sizeof(int)) != sizeof(int))
							fprintf(stderr, "write error\n");
					}
					if (un_lock(file_fd, -4, SEEK_CUR, 4) < 0)
						fprintf(stderr, "unlock error");
					FD_CLR(recent_fd, &check_set);
					id_occupied[id - 1] = 0;
				}
				else {
					id = atoi(requestP[recent_fd].buf);
					if (id_occupied[id  - 1] == 0) {
						if (write_lock(file_fd, (id - 1) * 2 * sizeof(int) + sizeof(int), SEEK_SET, 4) < 0) {
							write(requestP[recent_fd].conn_fd, "This account is occupied.\n", 26);
							FD_CLR(recent_fd, &check_set);
						}
						else {
							write(requestP[recent_fd].conn_fd, "This account is available.\n", 27);
							id_occupied[id - 1] = 1;
							requestP[recent_fd].account = id;
							continue;
						}
					}
					else {
						write(requestP[recent_fd].conn_fd, "This account is occupied.\n", 26);
						FD_CLR(recent_fd, &check_set);
					}
				}
#endif
				close(requestP[recent_fd].conn_fd);
				free_request(&requestP[recent_fd]);
			}
		}
		free(requestP);
		return 0;
}


// ======================================================================================================
// You don't need to know how the following codes are working
#include <fcntl.h>

static void* e_malloc(size_t size);


static void init_request(request* reqP) {
	reqP->conn_fd = -1;
	reqP->buf_len = 0;
	reqP->account = 0;
	reqP->wait_for_write = 0;
}

static void free_request(request* reqP) {
	/*if (reqP->filename != NULL) {
		free(reqP->filename);
		reqP->filename = NULL;
		}*/
	init_request(reqP);
}

// return 0: socket ended, request done.
// return 1: success, message (without header) got this time is in reqP->buf with reqP->buf_len bytes. read more until got <= 0.
// It's guaranteed that the header would be correctly set after the first read.
// error code:
// -1: client connection error
static int handle_read(request* reqP) {
	int r;
	char buf[512];

	// Read in request from client
	r = read(reqP->conn_fd, buf, sizeof(buf));
	if (r < 0) return -1;
	if (r == 0) return 0;
	char* p1 = strstr(buf, "\015\012");
	int newline_len = 2;
	// be careful that in Windows, line ends with \015\012
	if (p1 == NULL) {
		p1 = strstr(buf, "\012");
		newline_len = 1;
		if (p1 == NULL) {
			ERR_EXIT("this really should not happen...");
		}
	}
	size_t len = p1 - buf + 1;
	memmove(reqP->buf, buf, len);
	reqP->buf[len - 1] = '\0';
	reqP->buf_len = len-1;
	return 1;
}

static void init_server(unsigned short port) {
	struct sockaddr_in servaddr;
	int tmp;

	gethostname(svr.hostname, sizeof(svr.hostname));
	svr.port = port;

	svr.listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (svr.listen_fd < 0) ERR_EXIT("socket");

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(port);
	tmp = 1;
	if (setsockopt(svr.listen_fd, SOL_SOCKET, SO_REUSEADDR, (void*)&tmp, sizeof(tmp)) < 0) {
		ERR_EXIT("setsockopt");
	}
	if (bind(svr.listen_fd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
		ERR_EXIT("bind");
	}
	if (listen(svr.listen_fd, 1024) < 0) {
		ERR_EXIT("listen");
	}
}

static void* e_malloc(size_t size) {
	void* ptr;

	ptr = malloc(size);
	if (ptr == NULL) ERR_EXIT("out of memory");
	return ptr;
}

