#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

FILE *log_fp;
int urgent_due_time;
int very_urgent_due_time;
int num[3] = {0};
int ord_finish_num = 0;
int send_count = 0, finish_count = 0;

void handler_int(int signo)
{
	finish_count++;
	ord_finish_num++;
	fprintf(log_fp, "finish 0 %d\n", ord_finish_num);
	fflush(log_fp);
}

void handler1(int signo)
{
	finish_count++;
	urgent_due_time = -1;
	fprintf(log_fp, "finish 1 %d\n", num[1]);
	fflush(log_fp);
}

void handler2(int signo)
{
	finish_count++;
	very_urgent_due_time = -1;
	fprintf(log_fp, "finish 2 %d\n", num[2]);
	fflush(log_fp);
}

int main(int argc, char* argv[])
{
	FILE *data_fp;
	int type, send_time, i;
	int current_time = 0;
	struct sigaction sa_int, sa_1, sa_2;
	struct timespec timer; 
	urgent_due_time = -1;
	very_urgent_due_time = -1;
	data_fp = fopen(argv[1], "r");
	log_fp = fopen("sender_log", "w");
	sa_int.sa_handler = handler_int;
	sa_int.sa_flags = 0;
	sigemptyset(&sa_int.sa_mask);
	sa_1.sa_handler = handler1;
	sa_1.sa_flags = 0;
	sigemptyset(&sa_1.sa_mask);
	sa_2.sa_handler = handler2;
	sa_2.sa_flags = 0;
	sigemptyset(&sa_2.sa_mask);
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGUSR1, &sa_1, NULL);
	sigaction(SIGUSR2, &sa_2, NULL);
	while (fscanf(data_fp, "%d %d", &type, &send_time) != EOF) {
		while (current_time < send_time) {
			timer.tv_sec = 0;
			timer.tv_nsec = 100000000;
			while (nanosleep(&timer, &timer) == -1){};
			current_time++;
			if (very_urgent_due_time != -1 && current_time >= very_urgent_due_time) {
				fprintf(log_fp, "timeout 2 %d\n", num[2]);
				fflush(log_fp);
				exit(0);
			}
			if (urgent_due_time != -1 && current_time >= urgent_due_time) {
				fprintf(log_fp, "timeout 1 %d\n", num[1]);
				fflush(log_fp);
				exit(0);
			}
		}
		send_count++;
		if (type == 0) {
			num[0]++;
			fprintf(log_fp, "send 0 %d\n", num[0]);
			fflush(log_fp);
			printf("ordinary\n");
			fflush(stdout);
		} else {
			num[type]++;
			fprintf(log_fp, "send %d %d\n", type, num[type]);
			fflush(log_fp);
			if (type == 1) {
				if (urgent_due_time == -1)
					urgent_due_time = current_time + 10;
				kill(getppid(), SIGUSR1);
			} else if (type == 2) {
				if (very_urgent_due_time == -1)
					very_urgent_due_time = current_time + 3;
				kill(getppid(), SIGUSR2);
			}
		}
	}
	while (finish_count < send_count) {
		if (very_urgent_due_time != -1 && current_time >= very_urgent_due_time) {
			fprintf(log_fp, "timeout 2 %d\n", num[2]);
			fflush(log_fp);
			exit(0);
		}
		if (urgent_due_time != -1 && current_time >= urgent_due_time) {
			fprintf(log_fp, "timeout 1 %d\n", num[1]);
			fflush(log_fp);
			exit(0);
		}
		timer.tv_sec = 0;
		timer.tv_nsec = 100000000;
		while (nanosleep(&timer, &timer) == -1){};
		current_time++;
	}
	return 0;
}
