#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <time.h>

int num[3];
FILE *log_fp;
pid_t sender_pid;

void handler1(int signo)
{
	struct timespec timer;
	num[1]++;
	fprintf(log_fp, "receive 1 %d\n", num[1]);
	fflush(log_fp);
	timer.tv_sec = 0;
	timer.tv_nsec = 500000000;
	while (nanosleep(&timer, &timer) == -1){};
	fprintf(log_fp, "finish 1 %d\n", num[1]);
	fflush(log_fp);
	kill(sender_pid, SIGUSR1);
}

void handler2(int signo)
{
	struct timespec timer;
	num[2]++;
	fprintf(log_fp, "receive 2 %d\n", num[2]);
	fflush(log_fp);
	timer.tv_sec = 0;
	timer.tv_nsec = 200000000;
	while (nanosleep(&timer, &timer) == -1){};
	fprintf(log_fp, "finish 2 %d\n", num[2]);
	fflush(log_fp);
	kill(sender_pid, SIGUSR2);
}

void ter_handler(int signo)
{
	kill(sender_pid, SIGKILL);
	wait(NULL);
	fprintf(log_fp, "terminate");
	fflush(log_fp);
	exit(0);
}

int main(int argc, char *argv[])
{
	int pipe_fd[2];
	int i;
	FILE *send_fp;
	struct timespec timer;
	struct sigaction sa_1, sa_2, sa_ter;
	sa_1.sa_handler = handler1;
	sa_1.sa_flags = SA_RESTART;
	sigemptyset(&sa_1.sa_mask);
	sa_2.sa_handler = handler2;
	sa_2.sa_flags = SA_RESTART;
	sigemptyset(&sa_2.sa_mask);
	sigaddset(&sa_2.sa_mask, SIGUSR1);
	sa_ter.sa_handler = ter_handler;
	sa_ter.sa_flags = SA_RESTART;
	sigemptyset(&sa_ter.sa_mask);
	sigaddset(&sa_ter.sa_mask, SIGUSR1);
	sigaddset(&sa_ter.sa_mask, SIGUSR2);
	sigaction(SIGUSR1, &sa_1, NULL);
	sigaction(SIGUSR2, &sa_2, NULL);
	sigaction(SIGINT, &sa_ter, NULL);
	for (i = 0; i < 3; i++)
		num[i] = 0;
	if (pipe(pipe_fd) < 0)
		fprintf(stderr, "pipe error\n");
	if ((sender_pid = fork()) < 0)
		fprintf(stderr, "fork error\n");
	else if (sender_pid == 0) {
		dup2(pipe_fd[0], STDIN_FILENO);
		dup2(pipe_fd[1], STDOUT_FILENO);
		close(pipe_fd[0]);
		close(pipe_fd[1]);
		if (execl("./sender", "./sender", argv[1], (char *)0) < 0)
			fprintf(stderr, "exec error\n");
	}
	close(pipe_fd[1]);
	send_fp = fdopen(pipe_fd[0], "r");
	int n;
	char buf[15];
	log_fp = fopen("receiver_log", "w");
	while (1) {
		if (fscanf(send_fp, "%s", buf) == EOF) {
			fprintf(log_fp, "terminate");
			fflush(log_fp);
			wait(NULL);
			exit(0);	
		}
		else {
			num[0]++;
			fprintf(log_fp, "receive 0 %d\n", num[0]);
			fflush(log_fp);
			timer.tv_sec = 1;
			timer.tv_nsec = 0;
			while (nanosleep(&timer, &timer) == -1){};
			fprintf(log_fp, "finish 0 %d\n", num[0]);
			fflush(log_fp);
			kill(sender_pid, SIGINT);
		}
	}
	return 0;
}
