#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

typedef struct range
{
	int begin, end;//include begin, exclude end
	int *array;
}range;

typedef struct range_pair
{
	range a, b;
	int *array;
}range_pair;

void* sort(void *p)//run by thread
{
	range* r = (range *)(p);
	sort(r->array + r->begin, r->array + r->end);
	printf("Sorted %d elements.\n", r->end - r->begin);
	pthread_exit(NULL);
}

int sort_seg(int n, int seg_size, int width[], int *a)//sort each segment
{
	int i, num_seg;
	pthread_t thread[n / seg_size + 1];
	range range[n / seg_size + 1];
	for (i = 0; i < n / seg_size; i++) {
		range[i].begin = seg_size * i;
		range[i].end = seg_size * (i + 1);
		range[i].array = a;
		width[i] = seg_size;
		if (pthread_create(&thread[i], NULL, sort, &range[i]) < 0)
			fprintf(stderr, "thread_create error\n");
	}
	if (n % seg_size != 0) {
		num_seg = n / seg_size + 1;
		range[i].begin = (n / seg_size) * seg_size;
		range[i].end = n;
		range[i].array = a;
		width[i] = range[i].end - range[i].begin;
		if (pthread_create(&thread[i], NULL, sort, &range[i]) < 0)
			fprintf(stderr, "thread_create error\n");
	} else
		num_seg = n / seg_size;
	//join
	for (i = 0; i < num_seg; i++)
		pthread_join(thread[i], NULL);
	return num_seg;
}

void* merge(void *p)// run by thread
{
	range_pair *rp = (range_pair *)(p);
	int *buf = new int[rp->a.end - rp->a.begin + rp->b.end - rp->b.begin];
	int i = rp->a.begin, j = rp->b.begin, n = 0;
	int dup = 0;
	int v;
	//merge
	while (i < rp->a.end && j < rp->b.end) {
		if ((rp->array)[i] < (rp->array)[j]) {
			buf[n] = (rp->array)[i];
			i++;
			n++;
		} else if ((rp->array)[i] > (rp->array)[j]) {
			buf[n] = (rp->array)[j];
			j++;
			n++;
		} else {
			dup++;
			v = (rp->array)[i];
			for (; (rp->array)[i] == v && i < rp->a.end; i++, n++) 
				buf[n] = (rp->array)[i];
			for (; (rp->array)[j] == v && j < rp->b.end; j++, n++) 
				buf[n] = (rp->array)[j];
		}
	}
	//the rest numbers
	if (j == rp->b.end) {	
		int m, k;
		for (m = rp->b.end - 1, k = rp->a.end - 1; i <= k; k--, m--)
			(rp->array)[m] = (rp->array)[k];
	}
	for (i = 0; i < n; i++)
		(rp->array)[rp->a.begin + i] = buf[i];
	printf("Merged %d and %d elements with %d duplicates.\n", rp->a.end - rp->a.begin, rp->b.end - rp->b.begin, dup);
	delete [] buf;
	pthread_exit(NULL);
}

void merge_seg(int num_seg, int width[], int *a)//merge segments
{
	int current = 0, i = 0;
	range_pair range_pair[num_seg / 2];
	pthread_t thread[num_seg / 2];
	while (num_seg > 1) {
		for (current = 0, i = 0; i < num_seg / 2; i++) {
			range_pair[i].a.begin = current;
			current += width[i * 2];
			range_pair[i].a.end = current;
			range_pair[i].b.begin = current;
			current += width[i * 2 + 1];
			range_pair[i].b.end = current;
			range_pair[i].array = a;
			if (pthread_create(&thread[i], NULL, merge, &range_pair[i]) < 0)
				fprintf(stderr, "thread_create error\n");
		}
		for (i = 0; i < num_seg / 2; i++)
			width[i] = width[i * 2] + width[i * 2 + 1];
		if (num_seg % 2 == 1) 
			width[i] = width[num_seg - 1];
		for (i = 0; i < num_seg / 2; i++)
			pthread_join(thread[i], NULL);
		num_seg -= num_seg / 2;
	}
	return;
}

int main(int argc, char* argv[])
{
	int seg_size = atoi(argv[1]);
	int n = 0;
	int i, j;
	int num_seg;
	int *a;
	scanf("%d", &n);
	a = new int[n];
	for (i = 0; i < n; i++)
		scanf("%d", &a[i]);
	int width[n / seg_size + 1];
	num_seg = sort_seg(n, seg_size, width, a);
	merge_seg(num_seg, width, a);
	for (i = 0; i < n; i++)
		printf("%d ", a[i]);
	delete [] a;
	return 0;
}
