#include <stdio.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

typedef struct combination
{
	int a, b, c, d;
}combination;

int main(int argc, char** argv)
{
	int judge_num, player_num;
	combination competition[1820];
	int player_score[16] = {0}, player_index[16], tmp;
	int i, j, k, l, n;
	char buf[25];
	//store argv
	if (argc > 2) {
		judge_num = atoi(argv[1]);
		player_num = atoi(argv[2]);
	}
	else 
		fprintf(stderr, "argv error\n");
	//store all combination
	int com_num = 0;
	for (i = 1; i <= player_num - 3; i++)
		for (j = i + 1; j <= player_num - 2; j++)
			for (k = j + 1; k <= player_num - 1; k++)
				for (l = k + 1; l <= player_num; l++) {
					competition[com_num].a = i;
					competition[com_num].b = j;
					competition[com_num].c = k;
					competition[com_num].d = l;
					com_num++;
				}
	//create judge
	pid_t judge_pid[judge_num];
	pid_t pid;
	fd_set read_set, ready_read_set;
	int in_fd[judge_num][2], out_fd[judge_num][2];//in and out is in the vision of judge
	FD_ZERO(&read_set);//initialize the fd_sets
	for (i = 0; i < judge_num; i++) {
		//create pipe
		if (pipe(in_fd[i]) < 0)
			fprintf(stderr, "pipe error\n");
		if (pipe(out_fd[i]) < 0)
			fprintf(stderr, "pipe error\n");
		FD_SET(out_fd[i][0], &read_set);
		//fork
		if ((pid = fork()) < 0)
			fprintf(stderr, "fork error\n");
		else if (pid == 0) {// child call exec to execute judge
			close(in_fd[i][1]);
			close(out_fd[i][0]);
			dup2(in_fd[i][0], STDIN_FILENO);
			close(in_fd[i][0]);
			dup2(out_fd[i][1], STDOUT_FILENO);
			close(out_fd[i][1]);
			sprintf(buf, "%d", i + 1);
			if (execl("./judge", "./judge", buf, (char *)0) < 0)
				fprintf(stderr, "exec error\n");
		} else {
			judge_pid[i] = pid;
			close(in_fd[i][0]);
			close(out_fd[i][1]);
		}
	}
	// I/O multiplexing
	int judge_id, find = 0;
	for (i = 0; i < judge_num; i++) {
		sprintf(buf, "%d %d %d %d\n", competition[i].a, competition[i].b, competition[i].c, competition[i].d);
		n = strlen(buf);
		if (write(in_fd[i][1], buf, n) != n)
			fprintf(stderr, "write error\n");
	}
	for (j = 0; j < com_num; j++) {
		ready_read_set = read_set;
		if (select (70, &ready_read_set, NULL, NULL, NULL) < 0)
			fprintf(stderr, "select error\n");
		for(k = 0; k < judge_num && !find; k++) {
			if (FD_ISSET(out_fd[k][0], &ready_read_set)) {
				judge_id = k + 1;
				find = 1;
			}
		}
		find = 0;
		if (read(out_fd[judge_id - 1][0], buf, 5) < 0)
			fprintf(stderr, "read error\n");
		player_score[atoi(buf) - 1]--;
		if (i < com_num) {
			sprintf(buf, "%d %d %d %d\n", competition[i].a, competition[i].b, competition[i].c, competition[i].d);
			n = strlen(buf);
			if (write(in_fd[judge_id - 1][1], buf, n) != n)
				fprintf(stderr, "write error\n");
			i++;
		}
	}
	// wait child
	sprintf(buf, "0 0 0 0\n");
	n = strlen(buf);
	for (i = 0; i < judge_num; i++) {
		if (write(in_fd[i][1], buf, n) != n)
			fprintf(stderr, "write error\n");
		if (waitpid(judge_pid[i], NULL, 0) < 0)
			fprintf(stderr, "waitpid error\n");
	}
	//sort
	for (i = 0; i < player_num; i++)
		player_index[i] = i + 1;
	for (i = 0; i < player_num - 1; i++)
		for (j = i + 1; j < player_num; j++) {
			if (player_score[i] > player_score[j]) {
				tmp = player_score[i];
				player_score[i] = player_score[j];
				player_score[j] = tmp;
				tmp = player_index[i];
				player_index[i] = player_index[j];
				player_index[j] = tmp;
			} else if (player_score[i] == player_score[j]) {
				if (player_index[i] > player_index[j]) {	
					tmp = player_index[i];
					player_index[i] = player_index[j];
					player_index[j] = tmp;
				}
			}
		}
	printf("%d", player_index[0]);
	for (i = 1; i < player_num; i++)
		printf(" %d", player_index[i]);
	printf("\n");
	return 0;
}
