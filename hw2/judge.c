#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <signal.h>

void shuffle(int card[])
{
	int i, j, tmp;
	for (i = 0; i < 53; i++) {
		j = rand() % 53;
		tmp = card[i];
		card[i] = card[j];
		card[j] = tmp;
	}
	return;
}

int main(int argc, char** argv)
{
	int judge_id, player[4];
	int terminate = 0;
	FILE *fp[5];
	int card[53], player_card_num[4], player_key[4], card_id, given_card;
	int return_key, cheated, eliminated, exist_player_num;
	int i, n, j;
	int asker, giver;
	int exist_player[4];
	pid_t pid, player_pid[4];
	char buf[50], name[16], rand_key[8];
	char return_index;
	if (argc > 1)
		judge_id = atoi(argv[1]);
	else 
		fprintf(stderr, "argv error\n");
	//create & open fifo
	sprintf(name, "./judge%d.FIFO", judge_id);
	if (mkfifo(name, 0666) < 0)
		fprintf(stderr, "mkfifo error\n");
	fp[0] = fopen(name, "r+");
	for (i = 0; i < 4; i++) {
		sprintf(name, "./judge%d_%c.FIFO", judge_id, i + 'A');
		if (mkfifo(name, 0666) < 0)
			fprintf(stderr, "mkfifo error\n");
		fp[i + 1] = fopen(name, "w+");
	}
	//create cards
	srand(time(NULL) + getpid());
	card[0] = 0;
	for (j = 0; j < 4; j++) 
		for (i = 1; i <= 13; i++) 
			card[i + j * 13] = i;
	while (!terminate) {
		if (read(STDIN_FILENO, buf, sizeof(buf)) < 0)
			fprintf(stderr, "read error\n");
		sscanf(buf, "%d %d %d %d", &player[0], &player[1], &player[2], &player[3]);
		if (player[0] == 0 && player[1] == 0 && player[2] == 0 && player[3] == 0) {
			terminate = 1;
			continue;
		}
		shuffle(card);
		for (i = 0; i < 4; i++) {
			buf[0] = 'A' + i;
			buf[1] = '\0';
			player_key[i] = rand() % 65536;
			sprintf(rand_key, "%d", player_key[i]);
			if ((pid = fork()) < 0)
				fprintf(stderr, "fork error\n");
			else if (pid == 0) {
				if (execl("./player", "./player", argv[1], buf, rand_key, (char *)0) < 0)
					fprintf(stderr, "exec error\n");
			} else 
				player_pid[i] = pid;
		}
		//send cards to player and read from them
		for (i = 0; i < 4; i++) {
			if (i == 0) {
				fprintf(fp[i + 1], "%d %d %d %d %d %d %d %d %d %d %d %d %d %d\n", card[0], card[1], card[2], card[3], card[4], card[5], card[6], card[7], card[8], card[9], card[10], card[11], card[12], card[13]);
			} else {	
				fprintf(fp[i + 1], "%d %d %d %d %d %d %d %d %d %d %d %d %d\n", card[1 + i * 13], card[2 + i * 13], card[3 + i * 13], card[4 + i * 13], card[5 + i * 13], card[6 + i * 13], card[7 + i * 13], card[8 + i * 13], card[9 + i * 13], card[10 + i * 13], card[11 + i * 13], card[12 + i * 13], card[13 + i * 13]);
			}
			fflush(fp[i + 1]);
			cheated = 1;
			while (cheated) {
				fscanf(fp[0], " %c %d %d", &return_index, &return_key, &player_card_num[i]);
				if (return_key == player_key[return_index - 'A']) 
					cheated = 0;
				else 
					fprintf(stderr, "%c:return_key=%d, player_key=%d\n",return_index, return_key, player_key[i]);
			}
		}
		asker = 0;
		for (i = 0; i < 4; i++) 
			exist_player[i] = 1;
		exist_player_num = 4;
		if (player_card_num[0] == 0) {
			exist_player[0] = 0;
			exist_player_num = 3;
			asker = 1;
		}
		while (exist_player_num > 1) {
			for (i = 1; i < 4; i++) {
				if (exist_player[(asker + i) % 4]) {
					giver = (asker + i) % 4;
					break;
				}
			}
			//1.
			fprintf(fp[asker + 1], "< %d\n", player_card_num[giver]);
			fflush(fp[asker + 1]);
			//2.
			cheated = 1;
			while (cheated) {
				fscanf(fp[0], " %c %d %d", &return_index, &return_key, &card_id);
				if (asker != return_index - 'A')
					fprintf(stderr, "not correct asker\n");
				if (return_key == player_key[return_index - 'A']) 
					cheated = 0;
				else 
					fprintf(stderr, "%c cheated\n", return_index);
			}
			//3.
			fprintf(fp[giver + 1], "> %d\n", card_id);
			fflush(fp[giver + 1]);
			player_card_num[giver]--;
			if (player_card_num[giver] == 0) {
				exist_player[giver] = 0;
				exist_player_num--;
			}
			//4.
			cheated = 1;
			while (cheated) {
				fscanf(fp[0], " %c %d %d", &return_index, &return_key, &given_card);
				if (giver != return_index - 'A')
					fprintf(stderr, "not correct giver\n");
				if (return_key == player_key[return_index - 'A'])
					cheated = 0;
				else 
					fprintf(stderr, "%c cheated\n", return_index);
			}
			//5.
			fprintf(fp[asker + 1], "%d\n", given_card);
			fflush(fp[asker + 1]);
			//6.
			cheated = 1;
			while (cheated) {
				fscanf(fp[0], " %c %d %d", &return_index, &return_key, &eliminated);
				if (asker != return_index - 'A')
					fprintf(stderr, "not correct giver\n");
				if (return_key == player_key[return_index - 'A'])
					cheated = 0;
				else 
					fprintf(stderr, "%c cheated\n", return_index);
			}
			if (eliminated == 0)
				player_card_num[asker]++;
			else if (eliminated == 1)
				player_card_num[asker]--;
			else 
				fprintf(stderr, "eliminated error\n");
			if (player_card_num[asker] == 0) {
				exist_player[asker] = 0;
				exist_player_num--;
			}
			//change asker
			for (i = 1; i < 4; i++) {
				if (exist_player[(asker + i) % 4]) {
					asker = (asker + i) % 4;
					break;
				}
			}
		}
		for (i = 0; i < 4; i++) {
			if (exist_player[i] == 1) {
				sprintf(buf, "%d\n", player[i]);
				n = strlen(buf);
				if (write(STDOUT_FILENO, buf, n) != n)
					fprintf(stderr, "write error\n");
			}
			if (kill(player_pid[i], SIGKILL) < 0)
				fprintf(stderr, "kill error\n");
			if (waitpid(player_pid[i], NULL, 0) < 0)
				fprintf(stderr, "waitpid error\n");
		}
	}
	//remove fifo
	sprintf(name, "./judge%d.FIFO", judge_id);
	if (remove(name) < 0)
		fprintf(stderr, "remove error\n");
	for (i = 0; i < 4; i++) {
		sprintf(name, "./judge%d_%c.FIFO", judge_id, i + 'A');
		if (remove(name) < 0)
			fprintf(stderr, "remove error\n");
	}
	return 0;
}
