#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	int judge_id, rand_key;
	int card[14], card_num, giver_card_num, card_id, get_card, eliminated, exist_card[14];
	int i, j, n;
	char player_index, name[20], buf[50];
	int read_fd, write_fd;
	if (argc > 3) {
		judge_id = atoi(argv[1]);
		player_index = argv[2][0];
		rand_key = atoi(argv[3]);
	} else 
		fprintf(stderr, "argv error\n");
	sprintf(name, "./judge%d_%c.FIFO", judge_id, player_index);
	if ((read_fd = open(name, O_RDWR)) < 0)
		fprintf(stderr, "open error\n");
	sprintf(name, "./judge%d.FIFO", judge_id);
	if ((write_fd = open(name, O_RDWR)) < 0)
		fprintf(stderr, "open error\n");
	if (read(read_fd, buf, sizeof(buf)) < 0)
		fprintf(stderr, "read error\n");
	if (player_index == 'A') {
		card_num = 14;
		sscanf(buf, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d", &card[0], &card[1], &card[2], &card[3], &card[4], &card[5], &card[6], &card[7], &card[8], &card[9], &card[10], &card[11], &card[12], &card[13]);
	} else {	
		card_num = 13;
		sscanf(buf, "%d %d %d %d %d %d %d %d %d %d %d %d %d", &card[0], &card[1], &card[2], &card[3], &card[4], &card[5], &card[6], &card[7], &card[8], &card[9], &card[10], &card[11], &card[12]);
	}
	//make pair of cards
	int pair = 0;
	for (i = 0; i < card_num; i++)
		exist_card[i] = 1;
	for (i = 0; i < card_num - 1; i++) {
		pair = 0;
		for (j = i + 1; exist_card[i] && !pair && j < card_num; j++)
			if (exist_card[j] && card[i] == card[j]) {
				exist_card[i] = 0;
				exist_card[j] = 0;
				pair = 1;
			}
	}
	for (i = 0, j = 0; i < card_num; i++) {
		if (exist_card[i]) {
			card[j] = card[i];
			j++;
		}
	}
	card_num = j;
	sprintf(buf, "%c %d %d\n", player_index, rand_key, card_num);
	n = strlen(buf);
	if (write(write_fd, buf, n) != n)
		fprintf(stderr, "write error\n");
	//card_id is the id we want to draw
	srand(time(NULL) + getpid());
	while (1) {
		if (read(read_fd, buf, sizeof(buf)) < 0)
			fprintf(stderr, "read error\n");
		if (buf[0] == '<') {// draw card from others
			sscanf(buf, "< %d", &giver_card_num);
			card_id = (rand() % giver_card_num) + 1;
			sprintf(buf, "%c %d %d\n", player_index, rand_key, card_id);
			n = strlen(buf);
			if (write(write_fd, buf, n) != n) 
				fprintf(stderr, "write error\n");
		} else if (buf[0] == '>') {// be drew form others
			sscanf(buf, "> %d", &card_id);
			sprintf(buf, "%c %d %d\n", player_index, rand_key, card[card_id - 1]);
			for (i = card_id - 1; i < card_num - 1; i++)
				card[i] = card[i + 1];
			card_num--;
			n = strlen(buf);
			if (write(write_fd, buf, n) != n)
				fprintf(stderr, "write error\n");
		} else {//get card
			sscanf(buf, "%d", &get_card);
			eliminated = 0;
			for (i = 0; i < card_num && !eliminated; i++) {
				if (card[i] == get_card) {
					card_id = i + 1;
					eliminated = 1;
				}
			}
			if (eliminated) {
				for (i = card_id - 1; i < card_num - 1; i++)
					card[i] = card[i + 1];
				card_num--;
			} else {
				card[card_num] = get_card;
				card_num++;
			}
			sprintf(buf, "%c %d %d\n", player_index, rand_key, eliminated);
			n = strlen(buf);
			if ((n = write(write_fd, buf, n)) != n)
				fprintf(stderr, "write error\n");
		}
	}
	return 0;
}
